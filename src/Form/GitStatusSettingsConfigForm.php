<?php

namespace Drupal\git_status\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure git_status settings.
 */
class GitStatusSettingsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  const SETTINGS = 'git_status.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'git_status_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['repository_root'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Repository Root'),
      '#description' => $this->t('This is the absolute system path to the directory containing the project\'s git directory.'),
      '#default_value' => $config->get('repository_root') ?? (realpath(DRUPAL_ROOT . '/..')),
      '#required' => TRUE,
    ];

    $form['status_info_display'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Git Info to Display'),
      '#description' => $this->t('Git Status can display info about the location and state of this project\'s git repository.'),
      '#options' => [
        'repository_root' => $this->t(
          'The path to the this project\'s root directory.'
        ),
        'git_version' => $this->t(
          'The git binary version (like <code>@git_version</code>).',
          ['@git_version' => 'git --version']
        ),
        'git_branch' => $this->t(
          'The currently checked-out branch (like <code>@git_branch</code>)',
          ['@git_branch' => 'git rev-parse --abbrev-ref HEAD']
        ),
        'git_tag' => $this->t(
          'The tag(s) pointing to this commit (like <code>@git_tag</code>).',
          ['@git_tag' => 'git tag --points-at HEAD']
        ),
        'git_log' => $this->t(
          'The log entry for the current commit (like <code>@git_log</code>).',
          ['@git_log' => 'git log -n 1']
        ),
        'git_status' => $this->t(
          'The status of the current commit (like <code>@git_status</code>).',
          ['@git_status' => 'git status']
        ),
      ],
      '#default_value' => $config->get('status_info_display') ?? ['git_status'],
      // This field is required because it's literally the only thing this
      // module does!
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('repository_root', $form_state->getValue('repository_root'))
      ->set('status_info_display', $form_state->getValue('status_info_display'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function validateFormFieldRepositoryRoot(FormStateInterface $form_state) {
    // Vars for repository_root validation.
    $repository_root = $form_state->getValue('repository_root');
    $git_directory_path = sprintf('%s/.git', rtrim($repository_root, '/'));

    // Validate that repository_root contains a .git dir.
    //
    // We only set a warning for the user in case they're setting the value for
    // a different environment than the CURRENT envioronment.
    if (!is_dir($git_directory_path)) {
      $warning = $this->t(
        'The directory <code>@git_dir</code> does not exist. Please ensure that <strong>Repository Root</strong> is set to the right value.',
        ['@git_dir' => $git_directory_path]
      );
      $this->messenger()->addWarning($warning);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->validateFormFieldRepositoryRoot($form_state);
  }

}
